var gulp = require("gulp");
var src = "./src";
var dist ="./dist";
var port = 5000;
var path = {
    img : {
        src:src+"/images/**/*.*",
        dist:dist+"/images",
        exc:"!./src/images/sprite/*.*"
    },
    js : {
        src:src+"/js/main.js",
        dist:dist+"/js/"
    },
    scss :{
        src:src+"/sass/**/*.scss",
        dist:dist+"/css/",
        autoprefixer:dist+"/css/style.css"
    },
    sprite :{
        src:src+"/images/sprite/",
        dist:dist+"/images/sprite/",
        css:src+"/sass/vendor/"
    },
    html:{
        src:src+"/html/**/*.html",
        dist:dist+"/html/",
        exc:"!./dist/html/include/**/*.html"
    }
}

//공용
var browserSync = require("browser-sync").create();
var reload = browserSync.reload;
var newer = require("gulp-newer");

//브라우저 싱크
gulp.task("bs", function(){
    browserSync.init({
        /*proxy: "localhost"*/
        server: {
            baseDir: dist
            // index:"./html/index.html"
        },
        port:port,
        ui: {
            port: port+1
        }
    });
    gulp.watch(path.js.src,["min-js"]);
    gulp.watch(path.scss.src,["css"]);
    gulp.watch(path.sprite.src+"*.*",["sprite-icon"]);
    //gulp.watch([path.img.src, path.img.exc],["min-images"]);
    gulp.watch(path.html.src, ["html-include"]);
});


//이미지 스프라이트
var spritesmith = require("gulp.spritesmith");
gulp.task("sprite-icon",function(){
	var spriteData = gulp.src(path.sprite.src+"*.*")
		.pipe(spritesmith({
			imgName:"sprite.png",
			cssName:"_sprite.scss",
			padding:5,
			cssTemplate: "sprite.css.handlebars"
		}));
    spriteData.img.pipe(gulp.dest(path.sprite.dist));
	spriteData.css.pipe(gulp.dest(path.sprite.css));
});






var autoprefixer = require("gulp-autoprefixer");//벤더프리픽스 설정
var sourcemaps = require("gulp-sourcemaps");//소스맵 (css minify)
//gulp-sass
var sass = require("gulp-sass");
gulp.task("css", function () {
    return gulp.src(path.scss.src)
        .pipe(newer(path.scss.dist))
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: "compact"}).on("error", sass.logError))
        .pipe(autoprefixer({
            browsers: [
                "ie >= 7",
                "last 10 Chrome versions",
                "last 10 Firefox versions",
                "last 2 Opera versions",
                "iOS >= 7",
                "Android >= 4.1"
            ],
            cascade: true,
            remove: false
        }))
        .pipe(sourcemaps.write("./", {
            includeContent: true,
            sourceRoot: "./sass"
        }))
        .pipe(gulp.dest(path.scss.dist))
        .pipe(browserSync.stream({match: "**/*.css"}))
        .on("finish",reload);
});


//w3c 테스트
var w3cjs = require("gulp-w3cjs");
gulp.task("w3cjs", function () {
    gulp.src([path.html.dist+"**/*.html",path.html.exc])
        .pipe(w3cjs())
        .pipe(w3cjs.reporter());
});

//html include
var include = require("gulp-html-tag-include");
gulp.task("html-include", function() {
    return gulp.src(path.html.src)
        .pipe(include())
        .pipe(gulp.dest(path.html.dist))
});


//이미지 압축
var imagemin = require("gulp-imagemin");
gulp.task("min-images", function() {
    gulp.src([path.img.src, path.img.exc])
        .pipe(imagemin())
        .pipe(gulp.dest(path.img.dist))
        .on("finish",reload);
});

//js minify
var minify = require("gulp-minify");
gulp.task("min-js", function() {
    gulp.src(path.js.src)
        .pipe(minify({
            ext:{
                min:".min.js"
            }
        }))
        .pipe(gulp.dest(path.js.dist))
        .on("finish",reload)
});

gulp.task("default", ["html-include", "css", "min-js"]);

