$(document).ready(function(){

    if(BrowserDetect.browser == "Explorer"){
        $("html").addClass("ie"+BrowserDetect.version);
    }
    $("#topBanner img").click(function(){
        $("#topBanner").stop().animate({
            "margin-top":"-131px"
        },400,function(){
            $("#topBanner").remove();
        })
    });


    gnb();
    scrollTop();
    gotoSmart();
});







//브라우저 체크
var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;

            if (dataString.indexOf(data[i].subString) !== -1) {
                return data[i].identity;
            }
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index === -1) {
            return;
        }

        var rv = dataString.indexOf("rv:");
        if (this.versionSearchString === "Trident" && rv !== -1) {
            return parseFloat(dataString.substring(rv + 3));
        } else {
            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
        }
    },

    dataBrowser: [
        {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
        {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
        {string: navigator.userAgent, subString: "Opera", identity: "Opera"},
        {string: navigator.userAgent, subString: "OPR", identity: "Opera"},

        {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
        {string: navigator.userAgent, subString: "Safari", identity: "Safari"}
    ]
};

BrowserDetect.init();


function gnb(){
    var $eventHander = $("#gnb .layout-width");
    $eventHander.hover(function(){
        $("body").addClass("activeGNB");
    },function(){
        $("body").removeClass("activeGNB")
    })
}

function gotoSmart(){
    $("#gotoSmart").on("click",function(){
        $("body").addClass("activeSmart").removeClass("activeChk");
    })
    $("#gotoChk").on("click",function(){
        $("body").removeClass("activeSmart");
        setTimeout(function(){
        $("body").addClass("activeChk");
        },1100)
        $("html,body").scrollTop(0);
    })
}

function scrollTop(){
    var winScroll = 0;
    $("#btnTop").on("click",function(){
        $("html,body").stop().animate({scrollTop:0},1000,"easeInOutQuint")
      return false;
    })
    $(window).scroll(function(){
        winScroll = $(this).scrollTop();
        if(winScroll == 0){
            $("#btnTop").fadeOut();
        }else{
            $("#btnTop").fadeIn();
        }
    })
}

